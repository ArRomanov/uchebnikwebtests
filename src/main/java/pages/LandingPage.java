package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static url.Paths.LANDING;

public class LandingPage extends Page {

    public static final String FIELD_LOGIN_XPATH = "//input[@id='lpLogin']";
    public static final String FIELD_PSWD_XPATH = "//input[@id='lpPassword']";
    public static final String BUTTON_LOGIN_XPATH = "//button[@id='lpSubmit']";
    public static final String LINK_ANDROID_APP_XPATH = "//a[@class='lp-googleplay']";
    public static final String LINK_APPLE_APP_XPATH = "//a[@class='lp-appstore']";
    public static final String BUTTON_SUPPORT_XPATH = "//a[@class='button-support']";
    public static final String BUTTON_MANUALS_XPATH = "//a[contains(text(),'Инструкции')]";
    public static final String LINK_FACEBOOK_XPATH = "//a[contains(@href, 'facebook')]";
    public static final String LINK_VKONTAKTE_XPATH = "//a[contains(@href, 'vk.com')]";
    public static final String LINK_CLASSMATES_XPATH = "//a[contains(@href, 'ok.ru')]";
    public static final String LINK_POPUP_FORGOT_PASS_XPATH = "//a[@id='popupLink']";
    public static final String MODAL_FOR_FORGOTTEN_PASS = "//div[@class='popup popup-visible']";
    public static final String MANUAL_FOR_TEACHER_AND_STUDENT = "//a[.= 'Инструкция для родителя и учащегося']";
    public static final String MANUAL_FOR_EMPLOYEE = "//a[.= 'Инструкция для сотрудников']";
    public static final String LOGIN_OR_PSWD_IS_WRONG = "//p[@id='lpError' and @style='display: block;']";

    private LandingPage() {
        super();
    }

    public static LandingPage initPage() {
        return new LandingPage();
    }

    public LandingPage openPage() {
        open(LANDING);
        super.waitForElementVisibility(BUTTON_LOGIN_XPATH, 2);
        return this;
    }

    public CataloguePage authWithParameters(String login, String pswd) {
        $(By.xpath(FIELD_LOGIN_XPATH)).setValue(login);
        $(By.xpath(FIELD_PSWD_XPATH)).setValue(pswd);
        $(By.xpath(BUTTON_LOGIN_XPATH)).click();
        return new CataloguePage();
    }

    public Page openLinkInBgAndCheck(String link, String compareUrl) {
        super
                .openLinkInBackground(link)
                .switchToTab(1)
                .compareCurrentUrl(compareUrl);
        return this;
    }
}
