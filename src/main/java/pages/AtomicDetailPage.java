package pages;

import static com.codeborne.selenide.Selenide.open;
import static url.Paths.ATOMIC_DETAIL_PAGE;

public class AtomicDetailPage extends Page {

    public static final String ATOMIC_ID = "//div[contains(@class,'viewMeta')]/div[2]";

    public AtomicDetailPage() {
        super();
    }

    public static AtomicDetailPage initPage() {
        return new AtomicDetailPage();
    }

    public AtomicDetailPage openPage(String atomicId) {
        open(ATOMIC_DETAIL_PAGE + atomicId);
        super.waitForElementVisibility(ATOMIC_ID, 4);
        return this;
    }
}
