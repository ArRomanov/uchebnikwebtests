package pages;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.open;
import static helpers.DataForTests.TEST_FOLDER;
import static org.testng.Assert.assertEquals;
import static pages.AtomicDetailPage.ATOMIC_ID;
import static url.Paths.ADD_ATOMIC_BASE;

public class AddingAtomicPage extends Page {

    public static final String TEXT_FIELD_FOR_TEXT_ATOMIC = "//textarea[@type='text']";
    public static final String DROP_DOWN_MATERIAL_TYPE_FOR_ATOMIC = "//span[contains(text(), 'Тип материала')]";
    public static final String ITEM_FOR_TEXT_MATERIAL_TYPE = "//span[contains(text(),'Объяснение')]";
    public static final String ITEM_FOR_IMAGE_MATERIAL_TYPE = "//span[contains(text(),'Картина')]";
    public static final String ITEM_FOR_EXCEL_MATERIAL_TYPE = "//span[contains(text(),'Таблица MS Excel')]";
    public static final String SUBJECT_DROPDOWN = "//span[text()='Предмет']";
    public static final String ENGLISH_SUBJECT_FROM_DROPDOWN = "//span[text()='Английский язык']";
    public static final String EDUCATION_LEVEL_OOO_RADIO_BUT = "//label[contains(text(),'ООО')]/..";
    public static final String PRESS_KAS_BUTTON = "//span[text()='Добавить КЭС']/..";
    public static final String CHOICE_KAS = "//div[contains(@class,'checkbox')]/label/div";
    public static final String KAS_BUTT_FOR_ADDING_IN_MODAL = "//span[text()='Добавить']";
    public static final String MATERIAL_AUTHOR_CHECKBOX_CSS = "div[id='Являюсь автором материала-checkbox']";
    public static final String DOCUMENT_AUTHOR = "//div[contains(text(),'Автор документа')]/following-sibling::input";
    public static final String SOURCE_LINK = "//div[contains(text(),'Ссылка на первоисточник *')]/following-sibling::input";
    public static final String CHOOSE_FOLDER_BUTTON = "//span[text()='Выбрать папку']";
    public static final String MY_FOLDER_IN_MODAL = "//span[contains(text(),'Автотесты')]";
    public static final String SAVE_ATOMIC_BUTTON = "//span[text()='Сохранить и посмотреть']";
    public static final String UPLOAD_BUTTON = "//input[@type='file']";

    public static final String TEXT_FOR_ATOMIC = "This is user's text for new atomic";
    public static final String TEXT_FOR_SOURCE = "This is autotest";

    public AddingAtomicPage() {
        super();
    }

    public AddingAtomicPage openPage() {
        openAddTextAtomicPage();
        return this;
    }

    public static AddingAtomicPage initPage() {
        return new AddingAtomicPage();
    }

    public AddingAtomicPage openAddTextAtomicPage() {
        open(ADD_ATOMIC_BASE + "?materialType=text");
        return this;
    }

    public AddingAtomicPage openAddAudioAtomicPage() {
        open(ADD_ATOMIC_BASE + "?materialType=sound");
        return this;
    }

    public AddingAtomicPage openAddVidoeAtomicPage() {
        open(ADD_ATOMIC_BASE + "?materialType=video");
        return this;
    }

    public AddingAtomicPage openAddImageAtomicPage() {
        open(ADD_ATOMIC_BASE + "?materialType=image");
        return this;
    }

    public AddingAtomicPage openAddFileAtomicPage() {
        open(ADD_ATOMIC_BASE + "?materialType=file");
        return this;
    }

    public AddingAtomicPage uploadFile(String pathToFile) {
        super
                .uploadFile(UPLOAD_BUTTON, pathToFile)
                .waitForElementVisibility(TEXT_FIELD_FOR_TEXT_ATOMIC, 5);
        return this;
    }

    public AddingAtomicPage setDescription() {
        super.setTextInField(TEXT_FIELD_FOR_TEXT_ATOMIC, TEXT_FOR_ATOMIC);
        return this;
    }

    public AddingAtomicPage fillAuthorAndLinkSource() {
        super.setTextInField(SOURCE_LINK, TEXT_FOR_SOURCE);
        super.setTextInField(DOCUMENT_AUTHOR, TEXT_FOR_SOURCE);
        return this;
    }

    public AddingAtomicPage setMaterialType(String materialType) {
        super.selectDropDownItem(DROP_DOWN_MATERIAL_TYPE_FOR_ATOMIC, materialType);
        return this;
    }

    public AddingAtomicPage setSubjectAndKas() {
        super.selectDropDownItem(SUBJECT_DROPDOWN, ENGLISH_SUBJECT_FROM_DROPDOWN);
        super.clickLMB(EDUCATION_LEVEL_OOO_RADIO_BUT);
        try {
            TimeUnit.MILLISECONDS.sleep(800);
        } catch (InterruptedException e) {
            logger.info("Error of waiting");
            Thread.currentThread().interrupt();
        }
        super
                .clickLMB(PRESS_KAS_BUTTON)
                .clickLMB(CHOICE_KAS)
                .clickLMB(KAS_BUTT_FOR_ADDING_IN_MODAL);
        return this;
    }

    public AddingAtomicPage clickAuthorCheckbox() {
        super.executeJavaScript("document.querySelector(\"" + MATERIAL_AUTHOR_CHECKBOX_CSS + "\").click()");
        return this;
    }

    public AddingAtomicPage setFolderForSave() {
        super
                .clickLMB(CHOOSE_FOLDER_BUTTON)
                .clickLMB(MY_FOLDER_IN_MODAL);
        assertEquals($$x("//span[contains(@class,'header-')]").get(1).getText(), TEST_FOLDER);
        return this;
    }

    public AtomicDetailPage saveAndGoToAtomicDetail() {
        super
                .clickLMB(SAVE_ATOMIC_BUTTON)
                .waitForElementVisibility(ATOMIC_ID, 15);
        return new AtomicDetailPage();
    }
}
