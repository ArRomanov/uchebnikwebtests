package pages;

import static com.codeborne.selenide.Selenide.open;
import static url.Paths.CATALOGUE;

public class CataloguePage extends Page {

    public static final String CATALOGUE_XPATH = "//div[.='Каталог']";
    public static final String PLATE_OF_MATERIAL = "//div[contains(@class, 'item')]";
    public static final String CONFIRM_DEL_BTN_IN_POPUP = "//span[contains(text(),'Удалить')]";
    public static final String RATING_BLOCK_OF_PLATE = "//div[contains(@class,'rating')]";
    public static final String TITLE_BLOCK_OF_PLATE = "//div[contains(@class,'materialTitle')]";
    public static final String AUTHOR_BLOCK_OF_PLATE = "//div[contains(@class,'cardMetaAuthor')]";
    public static final String DATE_BLOCK_OF_PLATE = "//div[contains(@class,'cardMetaAuthor')]/../div[2]";

    //preview locators
    public static final String LABLE_OF_TEST_MATERIAL_IN_PREVIEW = "//div[contains(@class,'materialPreviewType') and (text()='Тест')]";
    public static final String LABLE_OF_TUTORIAL_MATERIAL_IN_PREVIEW = "//div[contains(@class,'materialPreviewType') and (text()='Книга')]";
    public static final String LABLE_OF_EUP_MATERIAL_IN_PREVIEW = "//div[contains(@class,'materialPreviewType') and (text()='ЭУП')]";
    public static final String LABLE_OF_LESSON_MATERIAL_IN_PREVIEW = "//div[contains(@class,'materialPreviewType') and (text()='Интерактивный урок')]";
    public static final String LABLE_OF_ATOMIC_MATERIAL_IN_PREVIEW = "//div[contains(@class,'materialPreviewType') and (text()='Атомик')]";
    public static final String LABLE_OF_APPS_MATERIAL_IN_PREVIEW = "//div[contains(@class,'materialPreviewType') and (text()='Приложение')]";
    public static final String DELETE_BTN_IN_PREVIEW_FOR_MATERIALS = "//button[contains(text(),'удалить')]";
    public static final String ON_MODERATION_BUTTON = "//button[.= 'На модерацию']";
    public static final String RUN_BTN_FOR_VIRTUAL_LAB = "//span[text()='Запустить']";
    public static final String DOWNLOAD_BTN_FOR_VIRTUAL_LAB = "//span[text()='Скачать файл']";


    public CataloguePage() {
        super();
    }

    public static CataloguePage initPage() {
        return new CataloguePage();
    }

    public CataloguePage openPage() {
        open(CATALOGUE);
        super.waitForElementVisibility(PLATE_OF_MATERIAL, 2);
        return this;
    }

    public CataloguePage sendFirstPlateOnModeration() {
        super
                .clickLMB(PLATE_OF_MATERIAL)
                .clickLMB(ON_MODERATION_BUTTON);
        return this;
    }

    public CataloguePage assertIsExistModerationBtnForFirstPlate() {
        super
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(ON_MODERATION_BUTTON);
        return this;
    }

    public CataloguePage assertIsNotExistModerationBtnForFirstPlate() {
        super
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatNotExist(ON_MODERATION_BUTTON);
        return this;
    }

    @Override
    public CataloguePage closeAllToasts() {
        super.closeAllToasts();
        return this;
    }

    @Override
    public CataloguePage clickLMB(String xpath) {
        super.clickLMB(xpath);
        return this;
    }
}
