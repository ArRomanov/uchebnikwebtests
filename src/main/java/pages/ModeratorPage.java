package pages;

import org.testng.Assert;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.open;
import static pages.CataloguePage.PLATE_OF_MATERIAL;
import static url.Paths.MODERATOR_MATERIALS;

public class ModeratorPage extends Page {

    public static final String ON_REVIEW_TAB = "//div[text()='На рассмотрении']";
    public static final String ACCEPTED_TAB = "//div[text()='Принятые']";
    public static final String REJECTED_TAB = "//div[text()='Отклоненные']";

    public static final String MODERATION_BTN = "//span[text()='Модерировать']";
    public static final String ACCEPT_RADIO_BTN = "//label[contains(@for, 'accepted')]";
    public static final String REJECT_RADIO_BTN = "//label[contains(@for, 'rejected')]";
    public static final String FOR_APROVAL_RADIO_BTN = "//label[contains(@for, 'for_approval')]";
    public static final String SAVE_MODERATION_STATUS_BTN = "//span[text()='Сохранить']";

    public static final String TEXT_AREA_REJECT_REASON = "//textarea[contains(@placeholder,'Причина отклонения')]";

    public static final String MATERIAL_ID_IN_PREVIEW = "//span[text()='ID']/../span[2]";

    public ModeratorPage() {
        super();
    }

    public static ModeratorPage initPage() {
        return new ModeratorPage();
    }

    public ModeratorPage openPage() {
        open(MODERATOR_MATERIALS);
        return this;
    }

    public ModeratorPage switchToOnReviewTab() {
        super.clickLMB(ON_REVIEW_TAB);
        return this;
    }

    public ModeratorPage switchToAcceptedTab() {
        super.clickLMB(ACCEPTED_TAB);
        return this;
    }

    public ModeratorPage switchToRejectedTab() {
        super.clickLMB(REJECTED_TAB);
        return this;
    }

    public ModeratorPage acceptFirstMaterialCard() {
        super.clickLMB(PLATE_OF_MATERIAL)
                .clickLMB(MODERATION_BTN)
                .clickLMB(ACCEPT_RADIO_BTN)
                .clickLMB(SAVE_MODERATION_STATUS_BTN);
        return this;
    }

    public ModeratorPage rejectFirstMaterialCard() {
        super.clickLMB(PLATE_OF_MATERIAL)
                .clickLMB(MODERATION_BTN)
                .clickLMB(REJECT_RADIO_BTN)
                .setTextInField(TEXT_AREA_REJECT_REASON, "Reason of rejecting")
                .clickLMB(SAVE_MODERATION_STATUS_BTN);
        return this;
    }

    public ModeratorPage inForAprovalFirstMaterialCard() {
        super.clickLMB(PLATE_OF_MATERIAL)
                .clickLMB(MODERATION_BTN)
                .clickLMB(FOR_APROVAL_RADIO_BTN)
                .clickLMB(SAVE_MODERATION_STATUS_BTN);
        return this;
    }

    public ModeratorPage compareFirstMaterialIdInCatalogue(String matchMaterialId) {
        super.clickLMB(PLATE_OF_MATERIAL);
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        Assert.assertEquals(matchMaterialId, super.getVisibleTextByElement(MATERIAL_ID_IN_PREVIEW));
        return this;
    }
}
