package pages;

import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.open;
import static url.Paths.USER_SETTINGS;

public class UserSettingsPage extends Page {

    public static final String STUDY_ORGANIZATION_HEADER = "//div[text()='Образовательное учреждение']";
    public static final String E_MAIL_HEADER = "//div[text()='Почта']";
    public static final String SUBJECTS_HEADER = "//div[text()='Предметы']";
    public static final String USED_QUOTA_HEADER = "//div[contains(text(),'Использовано')]";
    public static final String CHANGE_PASWD_HEADER = "//div[text()='Пароль']";
    public static final String CHANGE_AVATAR_HEADER = "//div[text()='Изображение']";
    public static final String UPLOAD_AVATAR_BUTTON = "//input[contains(@class,'uploadableImageInput')]";
    public static final String DELETE_AVATAR_BUTTON = "//span[text()='Удалить']";
    public static final String CHANGE_PSWD_BUTTON = "//button[text()='Изменить пароль']";
    public static final String SAVE_NEW_PASWD = "//span[text()='Сохранить']";
    public static final String CANCEL_CHANGE_PASWD = "//span[text()='Отмена']";

    public static final String ADD_SUBJECT_DROPDOWN = "//input[@placeholder='Выберите предмет']";
    public static final String FIRST_SUBJECT_IN_DROPDOWN = "//li[contains(@class,'selectItem')][1]";
    public static final String FIRST_SUBJECT_IN_ADDED_LIST = "//div[contains(@class,'settingsSubject') and not(contains(@class,'settingsSubjectRemoveAction'))][1]";
    public static final String DELETE_BTN_FOR_FIRST_SUBJECT = FIRST_SUBJECT_IN_ADDED_LIST + "/div";
    public static final String SUCCESFULL_SUBJECT_CHANGING_TOAST = "//div[text()='Предметы профиля успешно изменены']";

    public UserSettingsPage() {
        super();
    }

    public static UserSettingsPage initPage() {
        return new UserSettingsPage();
    }

    public UserSettingsPage openPage() {
        open(USER_SETTINGS);
        return this;
    }

    @Override
    public UserSettingsPage assertThatExist(String elementXpath) {
        super.assertThatExist(elementXpath);
        return this;
    }

    @Override
    public UserSettingsPage assertThatNotExist(String elementXpath) {
        super.assertThatNotExist(elementXpath);
        return this;
    }

    public UserSettingsPage addAndCheckOneSubject() {
        String subjectFromDropDown = super
                .waitForElementVisibility(FIRST_SUBJECT_IN_ADDED_LIST, 2)
                .clickLMB(ADD_SUBJECT_DROPDOWN)
                .getVisibleTextByElement(FIRST_SUBJECT_IN_DROPDOWN);
        super
                .clickLMB(FIRST_SUBJECT_IN_DROPDOWN)
                .assertThatExist("//div[contains(text(),'" + subjectFromDropDown + "')]")
                .assertThatExist(SUCCESFULL_SUBJECT_CHANGING_TOAST)
                .closeAllToasts();
        return this;
    }

    public UserSettingsPage deleteAndCheckOneSubject() {
        String subjectFromAddedList = super
                .waitForElementVisibility(FIRST_SUBJECT_IN_ADDED_LIST, 2)
                .getVisibleTextByElement(FIRST_SUBJECT_IN_ADDED_LIST);
        super
                .clickLMB(DELETE_BTN_FOR_FIRST_SUBJECT)
                .assertThatNotExist("//div[contains(text(),'" + subjectFromAddedList + "')]")
                .assertThatExist(SUCCESFULL_SUBJECT_CHANGING_TOAST)
                .closeAllToasts();
        return this;
    }

    @Override
    public UserSettingsPage clickLMB(String xpath) {
        super.clickLMB(xpath);
        return this;
    }

    public UserSettingsPage refreshPage() {
        Selenide.refresh();
        return this;
    }
}
