package pages.components;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Header {

    private Header() {
    }

    public static final String ADD_MATERIAL_BUTTON_XPATH = "//button[.='Добавить материал']";
    public static final String SEARCH_TEXT_FIELD = "//input[contains(@class, 'searchField')]";
    public static final String SEARCH_BUTTON = "//button[contains(@class, 'searchBtn')]";
    public static final String USER_BUTTON = "//button[contains(@class,'userBtn')]";

    //Roles
    public static final String PUBLISHER_PROFILE = "Менеджер издательства";
    public static final String TEACHER = "Учитель";
    public static final String METHODIST = "Методист";

    public static void findMaterialByUsingSearchField(String textForSearch) {
        $(By.xpath(SEARCH_TEXT_FIELD)).setValue(textForSearch);
        $(By.xpath(SEARCH_BUTTON)).click();
    }

    public static void switchRole(String profile) {
        $(By.xpath(USER_BUTTON)).click();
        $(By.xpath("//span[text()='" + profile + "']")).click();
    }
}
