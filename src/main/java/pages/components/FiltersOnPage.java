package pages.components;

import com.codeborne.selenide.Selenide;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class FiltersOnPage {

    private static FiltersOnPage filters;
    public static final String TYPE_ALL = "checkbox-Все";
    public static final String TYPE_TESTS = "checkbox-Тесты";
    public static final String TYPE_TUTORIAL = "checkbox-Учебники";
    public static final String TYPE_EUP = "checkbox-ЭУП";
    public static final String TYPE_ATOMIC = "checkbox-Доп. материалы";
    public static final String TYPE_LESSON = "checkbox-Сценарии уроков";
    public static final String TYPE_APPS = "checkbox-Приложения";
    public static final String VIRTUAL_LABS = "checkbox-Виртуальные лаборатории";

    private static final String CLEAR_FILTERS = "//button[contains(@class,'clearFilterBtn')]";
    private static final String ADDITIONAL_MATERIALS_CHOICE = "//input[@id='checkbox-Доп. материалы']/../../button";
    private static final String CLOSE_FILTER_MODAL = "//div[contains(@class,'filter')]/button";

    private FiltersOnPage() {
    }

    public static void choiceFilters(String... filters) {
        for (String filter : filters) {
            Selenide.executeJavaScript("document.getElementById('" + filter + "').click()");
        }
    }

    public static void filterAdditionalyMaterials(String... filters) {
        $x(ADDITIONAL_MATERIALS_CHOICE).click();
        for (String filter : filters) {
            Selenide.executeJavaScript("document.getElementById('" + filter + "').click()");
        }
        $x(CLOSE_FILTER_MODAL).click();
    }

    public static void clearAllFilters() {
        $x(CLEAR_FILTERS).click();
    }
}
