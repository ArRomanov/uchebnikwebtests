package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static helpers.DataForTests.TEACHER_LOGIN;
import static helpers.DataForTests.TEACHER_PASS;
import static java.net.URLDecoder.*;
import static java.util.logging.Level.SEVERE;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;
import static org.testng.Assert.assertEquals;
import static pages.AddingAtomicPage.UPLOAD_BUTTON;
import static pages.CataloguePage.CATALOGUE_XPATH;
import static url.Paths.AUDIO_FILE;
import static url.Paths.BASE_URL;

public class Page {

    protected static Logger logger = Logger.getLogger("DataBaseLogger");
    private static final String MESSAGE = "Something went wrong";

    public static final String MAIN_PAGE_LABEL = "//img[@id='library-logo-unique-identifier']";
    public static final String CLOSE_TOAST_LOCATOR = "//button[contains(@class,'toastClose')]";
    private static final String MESS = "It's OK";
    private static final String MESS1 = "Something went wrong: {0} ";

    public static void authorization() {
        open(BASE_URL);
        $x(LandingPage.FIELD_LOGIN_XPATH).setValue(TEACHER_LOGIN);
        $x(LandingPage.FIELD_PSWD_XPATH).setValue(TEACHER_PASS);
        $x(LandingPage.BUTTON_LOGIN_XPATH).click();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            logger.info(MESSAGE);
            Thread.currentThread().interrupt();
        }
        $x(MAIN_PAGE_LABEL).should(Condition.exist);
    }

    public Page setTextInField(String xpath, String text) {
        $x(xpath).setValue(text);
        return this;
    }

    public Page clickLMB(String xpath) {
        try {
            TimeUnit.MILLISECONDS.sleep(600);
        } catch (InterruptedException e) {
            logger.info(MESSAGE);
            Thread.currentThread().interrupt();
        }
        $x(xpath).click();
        return this;
    }

    public Page clickLMBByID(String id) {
        try {
            TimeUnit.MILLISECONDS.sleep(600);
        } catch (InterruptedException e) {
            logger.info(MESSAGE);
            Thread.currentThread().interrupt();
        }
        $(id(id)).click();
        return this;
    }

    public Page openLinkInBackground(String xpath) {
        $x(xpath).sendKeys(Keys.CONTROL, Keys.ENTER);
        return this;
    }

    public Page switchToTab(int numOfTab) {
        switchTo().window(numOfTab);
        return this;
    }

    public Page assertThatExist(String elementXpath) {
        $x(elementXpath).should(Condition.exist);
        return this;
    }

    public Page assertThatNotExist(String elementXpath) {
        $x(elementXpath).shouldNot(Condition.exist);
        return this;
    }

    public Page compareCurrentUrl(String matchUrl) {
        logger.log(SEVERE, MESS, url());
        logger.log(SEVERE, MESS, matchUrl);
        try {
            assertEquals(decode(url(), "utf-8"), matchUrl, "Current url don't compare with template");
        } catch (UnsupportedEncodingException e) {
            logger.log(SEVERE, MESS1, e);
        }
        return this;
    }

    public Page selectDropDownItem(String dropDownXpath, String itemXpath) {
        $x(dropDownXpath).click();
        $x(itemXpath).click();
        return this;
    }

    public String getVisibleTextByElement(String elementXpath) {
        return $(xpath(elementXpath)).getText();
    }

    public Page uploadFile(String inputXpath, String filePath) {
        // По умолчанию пытаемся грузить файл через selenoid
        // Если exception, значит тест выполняется локально (скорее всего) и загружаем файл обычным способом
        try {
            WebElement input = $(By.xpath(inputXpath));
            ((RemoteWebDriver) WebDriverRunner.getWebDriver()).setFileDetector(new LocalFileDetector());
            input.sendKeys(filePath);
        } catch (WebDriverException e) {
            $x(inputXpath).uploadFile(new File(filePath));
        }
        return this;
    }

    public Page waitForElementVisibility(String elementXpath, int seconds) {
        WebDriverWait wait = new WebDriverWait(getWebDriver(), seconds);
        wait.until(ExpectedConditions.presenceOfElementLocated(xpath(elementXpath)));
        return this;
    }

    public Page closeAllToasts() {
        this.waitForElementVisibility(CLOSE_TOAST_LOCATOR, 3);
        List listOfElements = $$x(CLOSE_TOAST_LOCATOR);
        for (Object element : listOfElements) {
            SelenideElement toastElement = (SelenideElement) element;
            toastElement.click();
        }
        return this;
    }

    public Page executeJavaScript(String jsCode) {
        Selenide.executeJavaScript(jsCode);
        return this;
    }

    public Page refreshPage() {
        Selenide.refresh();
        return this;
    }
}
