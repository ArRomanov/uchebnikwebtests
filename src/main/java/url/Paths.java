package url;

public class Paths {

    private Paths() {
    }

    public static final String SELENOID_HOST = "";

    public static final String BASE_URL = "";
    public static final String LANDING = "";
    public static final String CATALOGUE = "";
    public static final String ADD_ATOMIC_BASE = "";
    public static final String MY_MATERIALS = "";
    public static final String FAVORITES = "";
    public static final String ATOMIC_DETAIL_PAGE = "";
    public static final String USER_SETTINGS = "";

    public static final String MODERATOR_MATERIALS = "";

    public static final String VIEW_ATOMIC_URL = "";

    public static final String ANDROID_APP = "";
    public static final String APPLE_APP = "";

    public static final String HELP_FAQ = "";

    //local
    public static final String AUDIO_FILE = "src/test/resources/FilesForUpload/track.mp3";
    public static final String VIDEO_FILE = "src/test/resources/FilesForUpload/video.mp4";
    public static final String IMAGE_FILE = "src/test/resources/FilesForUpload/image.jpg";
    public static final String EXCEL_FILE = "src/test/resources/FilesForUpload/Book.xlsx";
}


