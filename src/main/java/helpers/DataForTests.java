package helpers;

public class DataForTests {

    private DataForTests() {
    }

    public static final String TEACHER_LOGIN;
    public static final String TEACHER_PASS;
    public static final String USER_ID;
    public static final String TEST_FOLDER;
    public static final String PROFILE_TEACHER_ID;
    public static final String PROFILE_METHODIST_ID;
    public static final String BROWSER;

    static {
        TEACHER_LOGIN = "";
        TEACHER_PASS = "";
        TEST_FOLDER = "";
        PROFILE_TEACHER_ID = "";
        PROFILE_METHODIST_ID = "";
        USER_ID = "";
        BROWSER = "";
    }
}