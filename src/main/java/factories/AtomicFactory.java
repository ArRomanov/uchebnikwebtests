package factories;

import io.restassured.builder.MultiPartSpecBuilder;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static factories.Bodies.getBodyForAtomic;
import static factories.Urls.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class AtomicFactory {

    private AtomicFactory() {
    }

    public static int getNewTextAtomic() {
        return given()
                .contentType(JSON)
                .header("Accept", "application/json")
                .headers(Auth.getTeacherCookie())
                .body(getBodyForAtomic("text").toString())
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().statusCode(201)
                .extract().path("id");
    }

    public static int getNewFileAtomic() {
        File excelFile;
        int atomicIdFile = -1;
        excelFile = new File("src/test/resources/FilesForUpload/Book.xlsx");
        atomicIdFile = given()
                .header("Accept", "application/vnd.cms-v1+json")
                .header("Content-Type", "multipart/form-data")
                .headers(Auth.getTeacherCookie())
                .multiPart(new MultiPartSpecBuilder(excelFile).fileName("autoUploadFile")
                        .controlName(excelFile.getName())
                        .mimeType("application/vnd.ms-excel")
                        .build())
                .when().post(STABLE_BASE_URL + CMS_ATOMIC_OBJECT)
                .then().statusCode(201)
                .extract().jsonPath().get("id");
        updateInfoForAtomicFile(atomicIdFile);
        return atomicIdFile;
    }

    private static void updateInfoForAtomicFile(int atomicId) {
        given()
                .header("Accept", "application/vnd.cms-v1+json")
                .header("Content-Type", JSON)
                .headers(Auth.getTeacherCookie())
                .body(getBodyForAtomic("file").toString())
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + atomicId)
                .then().statusCode(200);
    }

    public static int getNewTextAtomicForAproval() {
        int newAtomicId = getNewTextAtomic();
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "for_approval");
        given()
                .headers(Auth.getTeacherCookie())
                .contentType(JSON)
                .header("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + newAtomicId)
                .then().statusCode(200);
        return newAtomicId;
    }

    public static int getNewTextAtomicWithAcceptedStatus() {
        int newAtomicId = getNewTextAtomicForAproval();
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "accepted");
        given()
                .headers(Auth.getMethodistCookie())
                .contentType(JSON)
                .header("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + newAtomicId)
                .then().statusCode(200);
        return newAtomicId;
    }

    public static int getNewTextAtomicWithRejectedStatus() {
        int newAtomicId = getNewTextAtomicWithAcceptedStatus();
        Map<String, Object> body = new HashMap<>();
        body.put("moderation_status", "rejected");
        body.put("reject_reason", "This is reject");
        given()
                .headers(Auth.getMethodistCookie())
                .contentType(JSON)
                .header("Accept", "application/json")
                .body(body)
                .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + newAtomicId)
                .then().statusCode(200);
        return newAtomicId;
    }
}
