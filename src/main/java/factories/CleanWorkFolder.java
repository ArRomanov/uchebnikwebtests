package factories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static factories.Bodies.ROOT_FOLDER;
import static factories.Urls.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class CleanWorkFolder {

    private List getMaterialsFromMyFolder(String materialType) {
        return given()
                .headers(Auth.getMethodistCookie())
                .contentType(JSON)
                .header("Accept", "application/json")
                .param("folder_id", ROOT_FOLDER)
                .param("type", materialType)
                .when().get(STABLE_BASE_URL + CMS_API_MATERIALS)
                .then().statusCode(200)
                .extract().jsonPath().getList("material_id");
    }

    private void atomicToForApprovalModerationStatus(List listOfCmsMaterials) {
        Map<String, String> body = new HashMap<>();
        body.put("moderation_status", "for_approval");
        for (Object materialId : listOfCmsMaterials) {
            given()
                    .headers(Auth.getMethodistCookie())
                    .header("Accept", "application/json")
                    .contentType(JSON)
                    .body(body)
                    .when().patch(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + materialId)
                    .then().statusCode(200);
        }
    }

    public void deleteAllAtomicsFromMyFolder() {
        List listOfMaterials = getMaterialsFromMyFolder("atomic_objects");
        atomicToForApprovalModerationStatus(listOfMaterials);
        for (Object atomicId : listOfMaterials) {
            given()
                    .headers(Auth.getTeacherCookie())
                    .header("Accept", "application/json")
                    .contentType(JSON)
                    .when().delete(STABLE_BASE_URL + CMS_ATOMIC_OBJECT + "/" + atomicId)
                    .then().statusCode(204);
        }
    }
}
