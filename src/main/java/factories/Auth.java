package factories;

import io.restassured.response.Response;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.HashMap;
import java.util.Map;

import static factories.Urls.EOM_SESSIONS;
import static factories.Urls.STABLE_BASE_URL;
import static helpers.DataForTests.*;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

public class Auth {

    private Auth() {
    }

    private static String authToken = null;

    protected static Map getTeacherCookie() {
        Map<String, Object> teacherCookie = new HashMap<>();
        teacherCookie.put("user-id", USER_ID);
        teacherCookie.put("profile-id", PROFILE_TEACHER_ID);
        teacherCookie.put("auth-token", getAuthorizationToken(TEACHER_LOGIN, TEACHER_PASS));
        return teacherCookie;
    }

    protected static Map getMethodistCookie() {
        Map<String, Object> methodistCookie = new HashMap<>();
        methodistCookie.put("user-id", USER_ID);
        methodistCookie.put("profile-id", PROFILE_METHODIST_ID);
        methodistCookie.put("auth-token", getAuthorizationToken(TEACHER_LOGIN, TEACHER_PASS));
        return methodistCookie;
    }


    private static String getAuthorizationToken(String login, String password) {
        if (authToken == null) {
            Response response = sendAuthRequest(login, password);
            authToken = response.jsonPath().get("authentication_token");
        }
        return authToken;
    }

    private static Response sendAuthRequest(String login, String password) {
        Map<String, Object> bodyForEomAuth = new HashMap<>();
        bodyForEomAuth.put("login", login);
        bodyForEomAuth.put("password_hash", encryptPassword(password, true));
        bodyForEomAuth.put("password_hash2", encryptPassword(password, false));
        return given()
                .contentType(JSON)
                .headers("Accept", "application/json")
                .body(bodyForEomAuth)
                .when().post(STABLE_BASE_URL + EOM_SESSIONS)
                .then().statusCode(200)
                .extract().response();
    }

    private static String encryptPassword(String password, boolean useSalt) {
        if (useSalt) {
            return DigestUtils.md5Hex(password + "");
        } else return DigestUtils.md5Hex(password);
    }
}
