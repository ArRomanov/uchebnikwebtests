package factories;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Bodies {

    public static final String ROOT_FOLDER = "";
    public static final String COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE = "";
    public static final String EDUCATION_LEVEL_ID_HARD_CODE = "";
    public static final String SUBJECT_ID_HARD_CODE = "";
    public static final String STUDYING_LEVEL_ID_HARD_CODE = "";

    private Bodies() {
    }

    protected static JSONObject getBodyForAtomic(String contentType) {
        Map<String, Object> materialEducationAreasAttributesMap = new HashMap<>();
        materialEducationAreasAttributesMap.put("subject_id", SUBJECT_ID_HARD_CODE);
        materialEducationAreasAttributesMap.put("education_level_id", EDUCATION_LEVEL_ID_HARD_CODE);
        JSONArray materialEducationAreasAttributesJsonArray = new JSONArray();
        materialEducationAreasAttributesJsonArray.put(materialEducationAreasAttributesMap);

        JSONObject body = createBodyWithSpecialParameters(contentType);
        body.put("material_education_areas_attributes", materialEducationAreasAttributesJsonArray);
        body.put("controllable_item_ids", COMMON_CONTROLLABLE_ITEM_ID_HARD_CODE);
        body.put("studying_level_id", STUDYING_LEVEL_ID_HARD_CODE);
        body.put("description", "This is atomic created automatically");
        body.put("folder_ids", ROOT_FOLDER);
        body.put("published", true);
        body.put("time_to_study", "00:11:11");
        return body;
    }

    private static JSONObject createBodyWithSpecialParameters(String contentType) {
        JSONObject body = new JSONObject();
        switch (contentType) {
            case "text":
                body.put("logical_type_id", 3);
                body.put("caption", "This is text atomic");
                body.put("content_type", contentType);
                break;
            case "file":
                body.put("logical_type_id", 40);
                body.put("caption", contentType + "AutoUploadAtomic");
                body.put("content_type", contentType);
                break;
            default:
        }
        return body;
    }
}
