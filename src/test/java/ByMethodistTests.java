import factories.AtomicFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ModeratorPage;
import pages.components.FiltersOnPage;
import pages.components.Header;

import java.net.MalformedURLException;

import static pages.Page.authorization;
import static pages.components.FiltersOnPage.TYPE_ATOMIC;
import static pages.components.Header.METHODIST;

public class ByMethodistTests extends BaseTest {

    @BeforeClass
    public static void initSettings() throws InterruptedException, MalformedURLException {
        authorization();
        Header.switchRole(METHODIST);
    }

    @Test
    public void atomicFromForAprovalToAccept() {
        int atomicId = AtomicFactory.getNewTextAtomicForAproval();
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        ModeratorPage
                .initPage()
                .switchToOnReviewTab()
                .acceptFirstMaterialCard()
                .switchToAcceptedTab()
                .compareFirstMaterialIdInCatalogue(Integer.toString(atomicId))
                .closeAllToasts();
    }

    @Test
    public void atomicFromForAprovalToReject() {
        int atomicId = AtomicFactory.getNewTextAtomicForAproval();
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        ModeratorPage
                .initPage()
                .switchToOnReviewTab()
                .rejectFirstMaterialCard()
                .switchToRejectedTab()
                .compareFirstMaterialIdInCatalogue(Integer.toString(atomicId))
                .closeAllToasts();
    }

    @Test
    public void atomicFromRejectedToAccepted() {
        int atomicId = AtomicFactory.getNewTextAtomicWithRejectedStatus();
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        ModeratorPage
                .initPage()
                .switchToRejectedTab()
                .acceptFirstMaterialCard()
                .switchToAcceptedTab()
                .compareFirstMaterialIdInCatalogue(Integer.toString(atomicId))
                .closeAllToasts();
    }

    @Test
    public void atomicFromAcceptedToRejected() {
        int atomicId = AtomicFactory.getNewTextAtomicWithAcceptedStatus();
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        ModeratorPage
                .initPage()
                .switchToAcceptedTab()
                .rejectFirstMaterialCard()
                .switchToRejectedTab()
                .compareFirstMaterialIdInCatalogue(Integer.toString(atomicId))
                .closeAllToasts();
    }
}
