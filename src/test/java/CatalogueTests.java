import com.codeborne.selenide.CollectionCondition;
import factories.AtomicFactory;
import org.testng.annotations.Test;
import pages.CataloguePage;
import pages.Page;
import pages.components.FiltersOnPage;
import pages.components.Header;

import static com.codeborne.selenide.Selenide.$$x;
import static pages.CataloguePage.*;


public class CatalogueTests extends BaseTest {

    @Test
    public void infoOfPlate() {
        CataloguePage
                .initPage()
                .openPage()
                .assertThatExist(RATING_BLOCK_OF_PLATE)
                .assertThatExist(TITLE_BLOCK_OF_PLATE)
                .assertThatExist(AUTHOR_BLOCK_OF_PLATE)
                .assertThatExist(DATE_BLOCK_OF_PLATE);
    }

    @Test
    public void countOfMaterialsOnPage() {
        CataloguePage
                .initPage()
                .openPage();
        $$x(PLATE_OF_MATERIAL).shouldHave(CollectionCondition.size(20));
    }

    @Test
    public void findMaterial() {
        int atomicId = AtomicFactory.getNewTextAtomic();
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_ATOMIC);
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_ATOMIC_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void applyTutorialsFilters() {
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_TUTORIAL);
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_TUTORIAL_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void applyEUPFilters() {
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_EUP);
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_EUP_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void applyLessonsFilters() {
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_LESSON);
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_LESSON_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void applyTestsFilters() {
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_TESTS);
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_TEST_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void applyOtherMaterialsFilters() {
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_ATOMIC);
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_ATOMIC_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void applyAppsFilters() {
        Page currentPage = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_APPS);
        currentPage
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(CataloguePage.LABLE_OF_APPS_MATERIAL_IN_PREVIEW);
    }

    @Test
    public void checkVirtualLab() {
        CataloguePage page = CataloguePage
                .initPage()
                .openPage();
        FiltersOnPage.filterAdditionalyMaterials(FiltersOnPage.VIRTUAL_LABS);
        page
                .clickLMB(PLATE_OF_MATERIAL)
                .assertThatExist(RUN_BTN_FOR_VIRTUAL_LAB)
                .assertThatExist(DOWNLOAD_BTN_FOR_VIRTUAL_LAB);
    }
}
