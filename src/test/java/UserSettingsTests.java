import org.testng.annotations.Test;
import pages.UserSettingsPage;

import static pages.UserSettingsPage.*;

public class UserSettingsTests extends BaseTest {

    @Test
    public void checkAllBlockHeaderFromPage() {
        UserSettingsPage
                .initPage()
                .openPage()
                .assertThatExist(STUDY_ORGANIZATION_HEADER)
                .assertThatExist(E_MAIL_HEADER)
                .assertThatExist(SUBJECTS_HEADER)
                .assertThatExist(USED_QUOTA_HEADER)
                .assertThatExist(CHANGE_PASWD_HEADER)
                .assertThatExist(CHANGE_AVATAR_HEADER);
    }

    @Test
    public void checkAllButtonsExists() {
        UserSettingsPage
                .initPage()
                .openPage()
                .assertThatExist(UPLOAD_AVATAR_BUTTON)
                .assertThatExist(DELETE_AVATAR_BUTTON)
                .clickLMB(CHANGE_PSWD_BUTTON)
                .assertThatExist(SAVE_NEW_PASWD)
                .assertThatExist(CANCEL_CHANGE_PASWD);
    }

    @Test
    public void addAndDeleteSubject() {
        UserSettingsPage
                .initPage()
                .openPage()
                .addAndCheckOneSubject()
                .deleteAndCheckOneSubject();
    }


}
