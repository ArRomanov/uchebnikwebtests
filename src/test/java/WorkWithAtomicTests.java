import factories.AtomicFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.CataloguePage;
import pages.Page;
import pages.components.FiltersOnPage;
import pages.components.Header;

public class WorkWithAtomicTests extends BaseTest {

    @DataProvider
    public Object[][] getAllTypesAtomic() {
        int textAtomicId = AtomicFactory.getNewTextAtomic();
        int fileAtomicId = AtomicFactory.getNewFileAtomic();
        Object[][] atomicIds = {{textAtomicId}, {fileAtomicId}};
        return atomicIds;
    }

    @Test(dataProvider = "getAllTypesAtomic")
    public void deletingAnyAtomic(int atomicId) {
        Page cataloque = CataloguePage
                .initPage()
                .openPage();
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        FiltersOnPage.choiceFilters(FiltersOnPage.TYPE_ATOMIC);
        cataloque
                .waitForElementVisibility(CataloguePage.PLATE_OF_MATERIAL, 2)
                .clickLMB(CataloguePage.PLATE_OF_MATERIAL)
                .clickLMB(CataloguePage.DELETE_BTN_IN_PREVIEW_FOR_MATERIALS)
                .clickLMB(CataloguePage.CONFIRM_DEL_BTN_IN_POPUP);
    }


}
