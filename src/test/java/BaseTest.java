import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import factories.CleanWorkFolder;
import helpers.AllureReportListener;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import pages.components.Header;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static helpers.DataForTests.BROWSER;
import static pages.Page.authorization;
import static url.Paths.BASE_URL;
import static url.Paths.SELENOID_HOST;

@Listeners(AllureReportListener.class)
public class BaseTest {

    static final String SELENOID_BROWSER_VERS = "64.0";

    @BeforeSuite
    public void globalSettings() throws MalformedURLException {
        baseUrl = BASE_URL;
        new CleanWorkFolder().deleteAllAtomicsFromMyFolder();
        Selenoid.disable();  //for debugging on local computer
        //Selenoid.enable();     //for run on selenoid
        authorization();
    }

    @BeforeClass
    public static void initSettings() throws InterruptedException, MalformedURLException {
        Header.switchRole(Header.TEACHER);
        TimeUnit.SECONDS.sleep(1);
    }

    @AfterMethod
    public void closeAllTabs() {
        String handleOfCurrentTab = getWebDriver().getWindowHandle();
        Set<String> setOfHandle = getWebDriver().getWindowHandles();
        for (String handleOfWindow : setOfHandle) {
            if (!handleOfWindow.equals(handleOfCurrentTab)) {
                switchTo().window(handleOfWindow).close();
            }
        }
        switchTo().window(handleOfCurrentTab);
    }

    private static class Selenoid {

        private static void enable() throws MalformedURLException {
            final DesiredCapabilities caps = DesiredCapabilities.chrome();
            caps.setVersion(SELENOID_BROWSER_VERS);
            caps.setCapability("enableVNC", true);
            caps.setCapability("screenResolution", "1920x1080x24");
            RemoteWebDriver driver = new RemoteWebDriver(new URL(SELENOID_HOST), caps);
            driver.manage().window().setSize(new Dimension(1920, 1080));
            WebDriverRunner.setWebDriver(driver);
        }

        private static void disable() {
            Configuration.browser = BROWSER;
            getWebDriver().manage().window().setSize(new Dimension(1600, 1600));
            //Configuration.headless = true;
        }
    }
}
