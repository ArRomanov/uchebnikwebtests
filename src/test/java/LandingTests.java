import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LandingPage;

import java.net.MalformedURLException;

import static pages.LandingPage.LOGIN_OR_PSWD_IS_WRONG;
import static pages.components.Header.ADD_MATERIAL_BUTTON_XPATH;
import static url.Paths.*;

public class LandingTests extends BaseTest {

    @BeforeClass
    public static void initSettings() throws InterruptedException, MalformedURLException {
        //without authorization();
    }

    @Test
    public void authorizationNegative() {
        LandingPage
                .initPage()
                .openPage()
                .authWithParameters("test_teacher1", "qwerty")
                .assertThatNotExist(ADD_MATERIAL_BUTTON_XPATH)
                .assertThatExist(LOGIN_OR_PSWD_IS_WRONG);
    }

    @Test
    public void checkAndroidLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.LINK_ANDROID_APP_XPATH, ANDROID_APP);
    }

    @Test
    public void checkAppleLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.LINK_APPLE_APP_XPATH, APPLE_APP);
    }

    @Test
    public void checkHelpLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.BUTTON_SUPPORT_XPATH, HELP_FAQ);
    }

    @Test
    public void checkManualsLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.BUTTON_MANUALS_XPATH, HELP_FAQ);
    }

    @Test
    public void checkFaceBookLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.LINK_FACEBOOK_XPATH, "https://www.facebook.com/obrmos");
    }

    @Test
    public void checkVkLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.LINK_VKONTAKTE_XPATH, "https://vk.com/ditobr");
    }

    @Test
    public void checkClassmatesLink() {
        LandingPage
                .initPage()
                .openPage()
                .openLinkInBgAndCheck(LandingPage.LINK_CLASSMATES_XPATH, "https://ok.ru/ditobr");
    }

    @Test
    public void popupForgotPassword() {
        LandingPage
                .initPage()
                .openPage()
                .clickLMB(LandingPage.LINK_POPUP_FORGOT_PASS_XPATH)
                .assertThatExist(LandingPage.MODAL_FOR_FORGOTTEN_PASS)
                .assertThatExist(LandingPage.MANUAL_FOR_TEACHER_AND_STUDENT)
                .assertThatExist(LandingPage.MANUAL_FOR_EMPLOYEE);
    }
}
