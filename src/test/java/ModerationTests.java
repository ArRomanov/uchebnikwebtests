import factories.AtomicFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import pages.CataloguePage;
import pages.ModeratorPage;
import pages.components.FiltersOnPage;
import pages.components.Header;

import static pages.components.FiltersOnPage.TYPE_ATOMIC;

public class ModerationTests extends BaseTest {

    @Test
    public void canSendOnModeration() {
        int atomicId = AtomicFactory.getNewTextAtomic();
        String atomicIdToString = Integer.toString(atomicId);
        CataloguePage currentPage = CataloguePage.initPage();
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        Header.findMaterialByUsingSearchField(atomicIdToString);
        currentPage
                .assertIsExistModerationBtnForFirstPlate()
                .sendFirstPlateOnModeration()
                .assertIsNotExistModerationBtnForFirstPlate()
                .closeAllToasts();
        Header.switchRole(Header.METHODIST);
        Header.findMaterialByUsingSearchField(atomicIdToString);
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        ModeratorPage
                .initPage()
                .compareFirstMaterialIdInCatalogue(atomicIdToString);
    }

    @Test
    public void canNotSendOnModerationFileAtomic() {
        Header.switchRole(Header.TEACHER);
        int atomicId = AtomicFactory.getNewFileAtomic();
        CataloguePage currentPage = CataloguePage.initPage();
        FiltersOnPage.choiceFilters(TYPE_ATOMIC);
        Header.findMaterialByUsingSearchField(Integer.toString(atomicId));
        currentPage.assertIsNotExistModerationBtnForFirstPlate();
    }
}
