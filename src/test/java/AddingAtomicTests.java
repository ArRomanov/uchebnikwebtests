import org.testng.annotations.Test;
import pages.AddingAtomicPage;
import pages.Page;

import static pages.AtomicDetailPage.ATOMIC_ID;
import static url.Paths.*;

public class AddingAtomicTests extends BaseTest {

    @Test
    public void addTextAtomic() throws InterruptedException {
        Page createTextAtomic = AddingAtomicPage
                .initPage()
                .openAddTextAtomicPage()
                .setDescription()
                .setMaterialType(AddingAtomicPage.ITEM_FOR_TEXT_MATERIAL_TYPE)
                .setSubjectAndKas()
                .setFolderForSave()
                .saveAndGoToAtomicDetail();
        String textWithAtomicId = createTextAtomic.getVisibleTextByElement(ATOMIC_ID);
        String separatedId = textWithAtomicId.substring(4);
        createTextAtomic.compareCurrentUrl(VIEW_ATOMIC_URL + separatedId);
    }

    @Test(description="Добавление тектсового атомика не автором")
    public void addTextAtomicByAnotherAuthor() {
        Page createTextAtomic = AddingAtomicPage
                .initPage()
                .openAddTextAtomicPage()
                .setDescription()
                .setMaterialType(AddingAtomicPage.ITEM_FOR_TEXT_MATERIAL_TYPE)
                .setSubjectAndKas()
                .clickAuthorCheckbox()
                .fillAuthorAndLinkSource()
                .setFolderForSave()
                .saveAndGoToAtomicDetail();
        String textWithAtomicId = createTextAtomic.getVisibleTextByElement(ATOMIC_ID);
        String separatedId = textWithAtomicId.substring(4);
        createTextAtomic.compareCurrentUrl(VIEW_ATOMIC_URL + separatedId);
    }

    @Test
    public void addAudioAtomic() {
        Page createAudioAtomic = AddingAtomicPage
                .initPage()
                .openAddAudioAtomicPage()
                .uploadFile(AUDIO_FILE)
                .setDescription()
                .setSubjectAndKas()
                .setFolderForSave()
                .saveAndGoToAtomicDetail();
        String textWithAtomicId = createAudioAtomic.getVisibleTextByElement(ATOMIC_ID);
        String separatedId = textWithAtomicId.substring(4);
        createAudioAtomic.compareCurrentUrl(VIEW_ATOMIC_URL + separatedId);
    }

    @Test
    public void addVideoAtomic() {
        Page createVideoAtomic = AddingAtomicPage
                .initPage()
                .openAddVidoeAtomicPage()
                .uploadFile(VIDEO_FILE)
                .setDescription()
                .setSubjectAndKas()
                .setFolderForSave()
                .saveAndGoToAtomicDetail();
        String textWithAtomicId = createVideoAtomic.getVisibleTextByElement(ATOMIC_ID);
        String separatedId = textWithAtomicId.substring(4);
        createVideoAtomic.compareCurrentUrl(VIEW_ATOMIC_URL + separatedId);
    }

    @Test
    public void addImageAtomic() {
        Page createImageAtomic = AddingAtomicPage
                .initPage()
                .openAddImageAtomicPage()
                .uploadFile(IMAGE_FILE)
                .setMaterialType(AddingAtomicPage.ITEM_FOR_IMAGE_MATERIAL_TYPE)
                .setDescription()
                .setSubjectAndKas()
                .setFolderForSave()
                .saveAndGoToAtomicDetail();
        String textWithAtomicId = createImageAtomic.getVisibleTextByElement(ATOMIC_ID);
        String separatedId = textWithAtomicId.substring(4);
        createImageAtomic.compareCurrentUrl(VIEW_ATOMIC_URL + separatedId);
    }

    @Test
    public void addFileAtomic() {
        Page createFileAtomic = AddingAtomicPage
                .initPage()
                .openAddFileAtomicPage()
                .uploadFile(EXCEL_FILE)
                .setMaterialType(AddingAtomicPage.ITEM_FOR_EXCEL_MATERIAL_TYPE)
                .setDescription()
                .setSubjectAndKas()
                .setFolderForSave()
                .saveAndGoToAtomicDetail();
        String textWithAtomicId = createFileAtomic.getVisibleTextByElement(ATOMIC_ID);
        String separatedId = textWithAtomicId.substring(4);
        createFileAtomic.compareCurrentUrl(VIEW_ATOMIC_URL + separatedId);
    }
}
